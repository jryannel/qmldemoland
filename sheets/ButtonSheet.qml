import QtQuick 2.0
import Core.Controls 1.0

Item {
    width: 600
    height: 600

    Rectangle {
        anchors.fill: parent
        color: '#000000'
    }

    Column {
        anchors.fill: parent
        anchors.margins: 16
        spacing: 16
        Button {}
        Button { text: "One" }
        Button { text: "Another" }
    }
}
