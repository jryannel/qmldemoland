import QtQuick 2.0

Item {
    id: root
    width: backround.width
    height: backround.height

    property bool checked: false

    property string value: '90'

    signal clicked()

    Image {
        id: backround
        source: 'images/radio_button.png'
        anchors.centerIn: parent
    }
    Image {
        id: backround_pushed
        source: 'images/radio_button_pushed.png'
        anchors.centerIn: parent
        opacity: root.checked?1.0:0.0
        Behavior on opacity { NumberAnimation { duration: 100 } }
    }
    Image {
        source: 'images/led_inactive.png'
        x: 2
        y: 1
    }
    Image {
        source: 'images/led_active.png'
        opacity: root.checked?1.0:0.0
        Behavior on opacity { NumberAnimation { duration: 250 } }
    }
    MouseArea {
        id: area
        anchors.fill: parent
        onClicked: root.checked = !root.checked
    }
}
