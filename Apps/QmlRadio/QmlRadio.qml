import QtQuick 2.0
import QtGraphicalEffects 1.0

Item {
    width: 400
    height: 300

    Image {
        id: background
        source: 'images/background.png'
    }

    Image {
        id: tactileGrill
        source: 'images/tactile_grill.png'
        anchors.centerIn: parent
    }

    Image {
        id: rim
        source: 'images/display_rim.png'
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: parent.right
        anchors.rightMargin: 8
        Text {
            id: frequencyLabel
            width: 200
            text: Number(85.5 + (108.0 - 85.5)*knob.value).toFixed(1)
            color: '#00E5FF'
            anchors.centerIn: parent
            font.pixelSize: 64
            font.family: 'Roboto'
            font.weight: Font.Light
            opacity: 0.75
            visible: false
            horizontalAlignment: Text.AlignRight
        }
        Glow {
            anchors.fill: frequencyLabel
            source: frequencyLabel
            fast: true
            radius: 16
            color: '#00E5FF'
//            samples: 0
            spread: 0.5
            transparentBorder: true
        }
    }

    RotaryKnob {
        id: knob
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.leftMargin: 8
        onValueChanged: print(value)
    }

    Image {
        id: mode
        source: 'images/mode_button.png'
        anchors.bottom: rim.top
        anchors.horizontalCenter: rim.horizontalCenter
        anchors.bottomMargin: 8
    }

    Image {
        id: button_holder
        source: 'images/radio_button_holder.png'
        anchors.right: rim.right
        anchors.top: rim.bottom
        Row {
            anchors.fill: parent
            anchors.margins: 2
            spacing: 3
            RadioButton {
                anchors.verticalCenter: parent.verticalCenter
            }
            RadioButton {
                anchors.verticalCenter: parent.verticalCenter
            }
            RadioButton {
                anchors.verticalCenter: parent.verticalCenter
            }
        }

    }



}

