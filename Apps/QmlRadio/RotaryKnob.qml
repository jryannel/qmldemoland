import QtQuick 2.0

Item {
    id: knob
    rotation: 24
    width: ring.width
    height: ring.height
    property real knobRotation: 0

    property real value: (knobRotation%360)/360

    Behavior on knobRotation { SmoothedAnimation {} }
    Image {
        id: ring
        source: 'images/rotary_knob_outer_ring.png'
        anchors.centerIn: parent
    }
    Image {
        id: rotaryRidges
        source: 'images/rotary_knob_outer_ridges.png'
        anchors.centerIn: parent
        rotation: parent.knobRotation
    }
    Image {
        id: rotaryGradients
        source: 'images/rotary_knob_gradient.png'
        anchors.centerIn: parent
    }
    Item {
        width: parent.width
        height: parent.height
        rotation: parent.knobRotation
        Image {
            id: rotaryHole
            source: 'images/rotary_knob_hole.png'
            anchors.centerIn: parent
            anchors.horizontalCenterOffset: 60
            rotation: -parent.rotation
            opacity: 1.0
        }
    }

    MouseArea {
        anchors.fill: parent
        onWheel: knob.knobRotation += 15 * (wheel.angleDelta.y > 0?1:-1)
    }
}
