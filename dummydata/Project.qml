import QtQuick 2.0

QtObject {
    id: root
    function image(name) {
        return Qt.resolvedUrl('images/' + name + '.png')
    }

    function asset(name) {
        return Qt.resolvedUrl('assets/' + name)
    }
}
