import QtQuick 2.0
import Core.Panels 1.0

DemoPanelBase {
    id: root

    Rectangle {
        anchors.centerIn: parent
        width: 80
        height: 80
        color: '#ffffff'
        opacity: 0.5
    }
}
