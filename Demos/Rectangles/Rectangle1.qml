import QtQuick 2.0
import Core.Utils 1.0
import Core.Fields 1.0
import Core.Panels 1.0

DemoPanelBase {
    id: root

    Rectangle {
        x: (offsetPicker.xValue * demoItem.width) - width/2
        y: (offsetPicker.yValue * demoItem.height) - height/2
        width: 40
        height: 40
        color: '#ffffff'
        opacity: 0.5
    }

    PositionPicker {
        id: offsetPicker
        xValue: 0.5
        yValue: 0.5
    }

    controls: [
        Slider {
            caption: 'slider'
        },
        CheckBox {
            caption: "checkbox"
        },
        RadioButtonColumn {
            id: buttons
            caption: "radio button"
            value: 'option-1'
            RadioButton {
                caption: 'option-1'
                group: buttons
            }
            RadioButton {
                caption: 'option-2'
                group: buttons
            }
        }

    ]

}
