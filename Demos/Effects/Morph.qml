import QtQuick 2.0
import Core.Panels 1.0
import QtGraphicalEffects 1.0


DemoPanelBase {
    id: root

    Text {
        id: label
        anchors.centerIn: parent
        width: 240
        text: 'Left'
        color: 'white'
        font.pixelSize: 48
        opacity: 0.0
        horizontalAlignment: Text.AlignHCenter
    }

    DirectionalBlur {
        id: blur
        anchors.fill: label
        angle: 0
        length: 0
        samples: 24
        transparentBorder: true
        source: label
        opacity: 0.0
    }

    state: 'left'


    states: [
        State {
            name: "left"
            PropertyChanges { target: label; text: 'Left' }
            PropertyChanges { target: blur; length: 64; source: label; opacity: 1.0 }
        },
        State {
            name: "right"
            PropertyChanges { target: label; text: 'Right' }
            PropertyChanges { target: blur; length: 64; source: right; opacity: 1.0 }
        }
    ]

    transitions: [
        Transition {
            from: 'right'
            to: "left"
            SequentialAnimation {
                NumberAnimation { target: blur; properties: 'length,opacity'}
                PropertyAction { target: blur; property: "length"; value: 0 }
            }
        },
        Transition {
            from: 'left'
            to: "right"
            reversible: true
            SequentialAnimation {
                NumberAnimation { target: blur; properties: 'length,opacity'}
                PropertyAction { target: blur; property: "length"; value: 0 }
            }
        }
    ]
    MouseArea {
        anchors.fill: demoItem
        parent: demoItem
        onClicked: {
            print('clicked')
            if(root.state === 'left') {
                root.state = 'right'
            } else {
                root.state = 'left'
            }
        }
    }

}
