import QtQuick 2.0

ListModel {
    ListElement { name: 'Morph'; group: 'Effects'; source: 'Demos/Effects/Morph.qml' }
    ListElement { name: 'Rectangle1'; group: 'Rectangles'; source: 'Demos/Rectangles/Rectangle1.qml' }
    ListElement { name: 'Rectangle2'; group: 'Rectangles'; source: 'Demos/Rectangles/Rectangle2.qml' }
}
