import QtQuick 2.0
import Core.Panels 1.0

Item {
    id: root
    width: 1024
    height: 768

    Rectangle {
        id: background
        anchors.fill: parent
        color: '#000000'
    }

    DemoListPanel {
        id: demoList
        anchors.top: root.top
        anchors.bottom: root.bottom
        anchors.left: root.left
        model: DemoListModel {}
        onOpen: {
            print('open demo: ' + entry.source)
            var args = {
                title: entry.name
            }
            demoLoader.setSource(entry.source, args)
        }
    }

    Loader {
        id: demoLoader
        anchors.left: demoList.right
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
    }
}

