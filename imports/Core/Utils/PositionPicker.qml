import QtQuick 2.0

Item {
    id: root
    x: parent.width * xValue - root.width/2
    y: parent.height * yValue - root.height / 2
    width: 8
    height: 8


    property real xValue: 0
    property real yValue: 0

    property alias pressed: area.pressed

    Rectangle {
        anchors.fill: parent
        color: '#999'
        opacity: pressed?0.5:1.0
    }

    MouseArea {
        id: area
        anchors.fill: parent

        property real startX
        property real startY

        onPressed: {
            startX = mouseX
            startY = mouseY
        }

        onPositionChanged: {
            var xval = (root.x + root.width / 2 + mouseX - startX) / root.parent.width
            var yval = (root.y + root.height / 2 + mouseY - startY) / root.parent.height
            xValue = Math.max(0, Math.min(xval, 1.0))
            yValue = Math.max(0, Math.min(yval, 1.0))
        }
    }
}
