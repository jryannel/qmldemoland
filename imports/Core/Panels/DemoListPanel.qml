import QtQuick 2.0

Item {
    id: root
    width: 160
    height: 480

    signal open(var entry)
    property alias model: view.model

    Rectangle {
        anchors.right: parent.right
        width: 1
        height: parent.height
        color: '#ffffff'
        opacity: 0.2
    }

    ListView {
        id: view
        anchors.fill: parent
        delegate: delegateItem
        section.property: 'group'
        section.criteria: ViewSection.FullString
        section.delegate: sectionItem
    }
    Component {
        id: sectionItem
        Item {
            width: parent.width
            height: 20
            Text {
                text: section
                anchors.fill: parent
                anchors.leftMargin: 12
                verticalAlignment: Text.AlignVCenter
                color: '#ffffff'
                font.bold: true
                font.pixelSize: 12
            }
        }
    }

    Component {
        id: delegateItem
        Item {
            width: ListView.view.width
            height: 20
            Rectangle {
                anchors.fill: parent
                color: '#333333'
                height: parent.height
            }
            Text {
                anchors.fill: parent
                anchors.leftMargin: 24
                verticalAlignment: Text.AlignVCenter
                text: model.name
                color: '#ffffff'
                font.family: "Arial"
                font.pixelSize: 12
            }
            MouseArea {
                anchors.fill: parent
                onClicked: root.open(model)
            }
        }
    }
    Component.onCompleted: {
        open(view.model.get(0))
    }
}
