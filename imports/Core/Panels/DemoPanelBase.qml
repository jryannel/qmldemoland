import QtQuick 2.0
import Core.Utils 1.0

Item {
    id: root
    width: 480
    height: 480

    property alias demoItem: demoContainer
    default property alias demoChildren: demoContainer.children
    property alias controls: controlContainer.children
    property string title

    Item {
        id: titleBar
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.left: parent.left
        height: 20
        Text {
            anchors.fill: parent
            anchors.leftMargin: 24
            verticalAlignment: Text.AlignVCenter
            font.family: 'Arial'
            font.pixelSize: 12
            color: '#ffffff'
            text: root.title
        }
        Tracer {}
    }

    Item {
        id: demoContainer
        property real margin: 24
        x: ( parent.width - controlContainer.width - width) / 2
        width: Math.min(parent.height - margin, parent.width - controlContainer.width -margin)
        height: width
        anchors.verticalCenter: parent.verticalCenter
        Tracer {}
    }
    Column {
        id: controlContainer
        anchors.top: titleBar.bottom
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        width: 300
    }
}
