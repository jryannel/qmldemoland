import QtQuick 2.0
// todo: shall be only the marker and a radio button column shall
// contain text and column extension
Item {
    id: root
    width: parent.width
    height: 20
    property string caption
    property string value: caption
    property RadioButtonColumn group
    property bool checked: value === group.value
    property real cellWidth: width/12
    property alias pressed: area.pressed
    onPressedChanged: {
        if(pressed) {
            buttons.value = value
        }
    }

    Row {
        id: content
        anchors.fill: parent
        Spacer { width: cellWidth }
        Item {
            id: box
            width: cellWidth
            height: 20
            Rectangle {
                id: outer
                width: 12; height: width
                anchors.centerIn: parent
                radius: width/2
                color: '#666'
                Rectangle {
                    id: inner
                    width: 6; height: 6
                    radius: width/2
                    anchors.centerIn: parent
                    color: root.checked?'#ccc':'black'
                }

            }
        }
        Spacer { width: cellWidth }
        Text {
            id: captionLabel
            width: 8*cellWidth
            anchors.verticalCenter: parent.verticalCenter
            horizontalAlignment: Text.AlignLeft
            text: root.caption
            font.family: 'Arial'
            font.pixelSize: 11
            color: '#B3B3B3'
        }
        Spacer { width: cellWidth }
    }
    MouseArea {
        id: area
        anchors.fill: parent
        preventStealing: true
    }
}
