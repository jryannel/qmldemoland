import QtQuick 2.0

Item {
    id: root
    width: parent.width
    height: 20
    property string caption
    property bool checked
    property real cellWidth: width/24
    property alias pressed: area.pressed

    Row {
        id: content
        anchors.fill: parent
        Text {
            id: captionLabel
            width: 8*cellWidth
            anchors.verticalCenter: parent.verticalCenter
            horizontalAlignment: Text.AlignRight
            text: root.caption + ':'
            font.family: 'Arial'
            font.pixelSize: 11
            color: '#B3B3B3'
        }
        Spacer { width: 4* cellWidth }
        Spacer { width: cellWidth }
        Item {
            id: box
            width: cellWidth
            height: 20
            Rectangle {
                id: outer
                width: 12; height: width
                anchors.centerIn: parent
                color: '#666'
                Rectangle {
                    id: inner
                    width: 6; height: 6
                    anchors.centerIn: parent
                    color: root.checked?'#ccc':'black'
                }
            }

            MouseArea {
                id: area
                anchors.fill: parent
                onClicked: root.checked = !root.checked
            }
        }
        Spacer { width: cellWidth }
    }
}
