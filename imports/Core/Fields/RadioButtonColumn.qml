import QtQuick 2.0

import QtQuick 2.0
// todo: shall be only the marker and a radio button column shall
// contain text and column extension
Item {
    id: root
    width: parent.width
    height: buttons.height
    property string caption
    property string value
    property real cellWidth: width/24
    default property alias children: buttons.children


    Row {
        id: content
        anchors.left: parent.left
        anchors.right: parent.right
        height: buttons.height
        Text {
            id: captionLabel
            width: 8*cellWidth
            anchors.verticalCenter: parent.verticalCenter
            horizontalAlignment: Text.AlignRight
            text: root.caption + ':'
            font.family: 'Arial'
            font.pixelSize: 11
            color: '#B3B3B3'
        }
        Spacer { width: 4* cellWidth }
        Column {
            id: buttons
            width: 12* cellWidth
        }
    }
}
