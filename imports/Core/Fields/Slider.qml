import QtQuick 2.0

Item {
    id: root
    property real value: 0.0
    property real maximum: 1.0
    property real minimum: 0.0
    property string caption
    property bool integer: false
    width: parent.width
    height: 20
    property real cellWidth: width/24

    property alias pressed: area.pressed

    function updatePosition() {
        if(maximum > minimum) {
            var pos = (track.width - handle.width) * (value - minimum) / (maximum - minimum)
            return Math.min(Math.max(pos, 0), track.width - handle.width)
        }
        return 0
    }


    Row {
        id: content
        anchors.fill: parent
        Text {
            id: captionLabel
            width: 8*cellWidth
            anchors.verticalCenter: parent.verticalCenter
            horizontalAlignment: Text.AlignRight
            text: root.caption + ':'
            font.family: 'Arial'
            font.pixelSize: 11
            color: '#B3B3B3'
        }
        Spacer { width: cellWidth }
        Text {
            id: valueLabel
            width: 3*cellWidth
            anchors.verticalCenter: parent.verticalCenter
            horizontalAlignment: Text.AlignLeft
            text: integer? root.value.toFixed(0) : root.value.toFixed(1)
            font.family: 'Arial'
            font.pixelSize: 11
            color: '#999999'
        }
        Spacer { width: cellWidth }
        Item {
            id: track
            height: parent.height
            width: 10*root.cellWidth
            Image {
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.margins: handle.width/2
                anchors.verticalCenter: parent.verticalCenter
                source: Project.image('slider_bg')
            }

            Image {
                anchors.left: parent.left
                anchors.right: handle.horizontalCenter
                anchors.margins: handle.width/2
                anchors.verticalCenter: parent.verticalCenter
                source: Project.image('slider_bg_active')
            }

            Image {
                id: handle
                width: 8; height: width
                anchors.verticalCenter: parent.verticalCenter
                x: updatePosition()
                source: Project.image('slider_handle' + (area.pressed?'_active':''))
            }


            MouseArea {
                id: area
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                height: 16
                preventStealing: true

                onPressed: {
                    updateValue()
                }

                onPositionChanged: {
                    if(pressed) {
                        updateValue()
                    }
                }

                function updateValue() {
                    var handleX = Math.max(0, Math.min(mouseX, area.width))
                    var realValue = (maximum - minimum) * handleX / area.width + minimum
                    value = root.integer? Math.round(realValue) : realValue
                }

            }
        }
        Spacer { width: cellWidth }
    }
}
