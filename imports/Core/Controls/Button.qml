import QtQuick 2.1

Item {
    id: root
    width: 80
    height: 20

    property alias text: label.text
    signal clicked()

    Rectangle {
        anchors.fill: parent
        color: area.pressed?'#333333':'transparent'
        border.color: '#B3B3B3'
        Behavior on color { ColorAnimation { duration: 75 } }
    }
    Text {
        id: label
        anchors.fill: parent
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        font.family: 'Arial'
        font.pixelSize: 11
        color: '#B3B3B3'
    }
    MouseArea {
        id: area
        anchors.fill: parent
        onClicked: root.clicked()
    }
}
